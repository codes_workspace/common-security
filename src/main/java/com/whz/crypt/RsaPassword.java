package com.whz.crypt;

import java.io.IOException;
import java.io.InputStream;

/**
 * openssl 生成的对应一组秘钥<br/>
 * -----BEGIN RSA PRIVATE KEY-----
 * -----END PUBLIC KEY-----
 *
 * @author zwh
 */
class RsaPassword extends StringPassword {

    RsaPassword(String password) {
        super(password);
    }

    RsaPassword(InputStream is) throws IOException {
        super("");
        passwd = FilePassword.read(is);
    }

    @Override
    public String getPassword() {
        return getRawKeys(passwd);
    }

    /**
     * 去除秘钥的注释部分
     */
    public static String getRawKeys(String keyContent) {
        if (keyContent != null) {
            String[] arrs = keyContent.split("\n");
            StringBuilder sb = new StringBuilder();
            for (String arr : arrs) {
                String tmp = arr.trim();
                if (!tmp.startsWith("-")) {
                    sb.append(tmp);
                }
            }
            return sb.toString();
        }
        return null;
    }
}